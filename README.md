# LAAC September 2022 - Waveform Tutorial

Tutorial for LIGO Academic Advisory Committee (LAAC) during the LVC September - 2022 meeting on generating waveforms using LALSuite. 

# Getting Started 
To start working on the notebook, you will have to go through the following steps which are:
1. Go to CIT (or Cardiff cluster if CIT is giving issues). 
2. Activate the conda environment already prepared. 
3. Create a kernel to be used with the jupyterlab. 
4. Log-in to jupyterlab! 

## CIT step
* Open a new terminal window. Do, 

        ligo-proxy-init albert.einstein

* SSH addresses:

        * CIT - ssh albert.einstein@ldas-grid.ligo.caltech.edu
        * Cardiff - ssh albert.einstein@ligo.gravity.cf.ac.uk
        
* Log in with your LIGO credentials. 
* Clone the conda environment: 

        conda activate /home/chinmay.kalaghatgi/.conda/envs/lalsuite-waveform-tutorial

* Create kernel for jupyter notebook with : 

        python -m ipykernel install --user --name=waveform-tutorial

* Clone this git page (and ignore the 2021 date ;) ).

        git clone git@git.ligo.org:chinmay-kalaghatgi/waveform-tutorial-laac21.git

## Jupyter lab step
* Go to [CIT Jupyter Lab](https://jupyter.ligo.caltech.edu/)  [Cardiff Jupyter Lab](https://ligo.gravity.cf.ac.uk/jupyter/user/albert.einstein/lab?)
* Log in with your 'albert.einstein' credentials. 
* Open the tutorial notebook from the cloned page.

